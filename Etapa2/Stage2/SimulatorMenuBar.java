package Stage2;

import javafx.scene.control.*;

public class SimulatorMenuBar extends MenuBar{
    SimulatorMenuBar (Simulator simulator){
        Menu controlMenu = new Menu("Control");
        Menu settings = new Menu("Settings");
        getMenus().add(controlMenu);
        getMenus().add(settings);
        MenuItem start = new MenuItem("Start");
        MenuItem stop = new MenuItem("Stop");
        controlMenu.getItems().addAll(start,stop);
        //Spinner(double min, double max, double initialValue)
        Spinner spinnerN=new Spinner(1,500,SimulatorConfig.N);
        MenuItem N = new MenuItem("N", spinnerN);
        Spinner spinnerI=new Spinner(0,499,SimulatorConfig.I);
        MenuItem I = new MenuItem("I", spinnerI);
        TextField textp0= new TextField(String.valueOf(SimulatorConfig.P0));
        MenuItem p0 = new MenuItem("p0",textp0 );

        settings.getItems().addAll(N,I,p0);
        start.setOnAction(e->SettingsDisable1(settings, simulator, spinnerN, spinnerI, textp0));//Start
        //start.setOnAction(e->simulator.start());//reinicie simulacion);
        //stop.setOnAction(e2->simulator.stop());//pare simulacion);
        stop.setOnAction(e->SettingsDisable2(settings, simulator));//Stop
    }
    public void SettingsDisable1(Menu settings, Simulator simulator, Spinner N, Spinner I, TextField p0){//Start
        SimulatorConfig.N = (Double)N.getValue();
        SimulatorConfig.I = (Double)I.getValue();
        SimulatorConfig.P0 = Double.parseDouble(p0.getText());

        simulator.start();
        settings.setDisable(true);


    }
    public void SettingsDisable2(Menu settings, Simulator simulator){//Stop
        simulator.stop();
        settings.setDisable(false);
    }
}

