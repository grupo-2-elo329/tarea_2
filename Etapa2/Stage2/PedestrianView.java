package Stage2;

import javafx.animation.Animation;
import javafx.animation.FillTransition;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.util.Duration;

import java.io.File;


public class PedestrianView {
    private final Pedestrian person;
    private Shape view;
    //private Rectangle view3;
    //private Circle view2;
    private final double SIZE = 25;
    public PedestrianView(Comuna comuna, Pedestrian p) {
        person = p;
        //de momento
        switch (p.getStatus()) {
            case 0 -> {
                view = new Rectangle(person.getX() - SIZE / 2, person.getY() - SIZE / 2, SIZE, SIZE);
                view.setFill(Color.BLUE);
            }
            case 1 -> view = new Circle(person.getX() - SIZE / 2, person.getY() - SIZE / 2, SIZE, Color.RED);
            case 2 -> {
                view = new Rectangle(person.getX() - SIZE / 2, person.getY() - SIZE / 2, SIZE, SIZE);
                view.setFill(Color.BROWN);
            }
        }

        comuna.getView().getChildren().addAll(view);

    }
    public void update(Comuna comuna) {
        switch(person.getNextStatus()) {
            case 0:
                Rectangle v = (Rectangle) view;
                v.setX(person.getX()); // done
                v.setY(person.getY()); // done
                break;
            case 1:
                if(person.getStatus() == 0){
                    view = new Circle(person.getX(), person.getY(), SIZE, Color.RED);
                } else  {
                    comuna.getView().getChildren().remove(view);
                    view = new Circle(person.getX(), person.getY(), SIZE, Color.RED);
                    Circle c = (Circle) view;
                    c.setCenterX(person.getX());
                    c.setCenterY(person.getY());
                    //comuna.getView().getChildren().remove(view);
                    comuna.getView().getChildren().addAll(c);
                    FillTransition ft = new FillTransition(Duration.millis(15000), c, Color.rgb(220,20,60),
                            Color.rgb(250,128,114));//testiar mas colores y tiempo
                    ft.setCycleCount(Animation.INDEFINITE);
                    ft.setAutoReverse(true);
                    ft.play();
                }
                break;
            case 2:
                //System.out.println("ME CURE PERRAS");
                comuna.getView().getChildren().remove(view);
                view = new Rectangle(person.getX(), person.getY(), SIZE, SIZE);
                view.setFill(Color.BROWN);
                Rectangle v2 = (Rectangle) view;
                v2.setX(person.getX()); // done
                v2.setY(person.getY()); // done
                comuna.getView().getChildren().addAll(v2);
                break;
        }

    }
}
