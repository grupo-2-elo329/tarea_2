package Stage1;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class ComunaView extends Group {
    private final Comuna comuna;
    Rectangle territoryView;
    public ComunaView(Comuna c){
        comuna = c;
        territoryView = new Rectangle(comuna.getWidth(), comuna.getHeight(), Color.WHITE);

        territoryView.setStroke(Color.BROWN);
        getChildren().add(territoryView);
        setFocusTraversable(true);  // needed to receive mouse and keyboard events.
    }
    public void update(){
        Pedestrian p = comuna.getPedestrian();
        p.updateView();
    }
    public Rectangle getTerritoryView() {
        return territoryView;
    }

    public void  refresh(Rectangle prev_territory){
        getChildren().remove(prev_territory);
        this.territoryView = new Rectangle(comuna.getWidth(), comuna.getHeight(), Color.WHITE);
        territoryView.setStroke(Color.BROWN);
        getChildren().add(territoryView);
        setFocusTraversable(true);  // needed to receive mouse and keyboard events.
    }
}
