package Stage3;

import javafx.scene.control.*;

public class SimulatorMenuBar extends MenuBar{
    SimulatorMenuBar (Simulator simulator){
        Menu controlMenu = new Menu("Control");
        Menu settings = new Menu("Settings");
        getMenus().add(controlMenu);
        getMenus().add(settings);
        MenuItem start = new MenuItem("Start");
        MenuItem stop = new MenuItem("Stop");
        controlMenu.getItems().addAll(start,stop);
        Slider sliderM = new Slider();
        sliderM.setMin(0);
        sliderM.setMax(1);
        sliderM.setValue(SimulatorConfig.M);
        sliderM.setShowTickLabels(true);
        sliderM.setShowTickMarks(true);
        sliderM.setMajorTickUnit(0.1);
        MenuItem M = new MenuItem("M", sliderM);

        Spinner spinnerN=new Spinner(1,500, SimulatorConfig.N);
        MenuItem N = new MenuItem("N", spinnerN);
        Spinner spinnerI=new Spinner(0,499, SimulatorConfig.I);
        MenuItem I = new MenuItem("I", spinnerI);
        TextField textp0= new TextField(String.valueOf(SimulatorConfig.P0));
        MenuItem p0 = new MenuItem("P0",textp0 );

        TextField textp1 = new TextField(String.valueOf(SimulatorConfig.P1));
        MenuItem p1 = new MenuItem("P1",textp1);

        TextField textp2 = new TextField(String.valueOf(SimulatorConfig.P2));
        MenuItem p2 = new MenuItem("P2",textp2);

        settings.getItems().addAll(M,N,I,p0,p1,p2);
        start.setOnAction(e->SettingsDisable1(settings, simulator,sliderM, spinnerN, spinnerI, textp0, textp1, textp2));//Start
        stop.setOnAction(e->SettingsDisable2(settings, simulator));//Stop

    }
    public void SettingsDisable1(Menu settings, Simulator simulator,Slider M, Spinner N, Spinner I, TextField p0,
                                 TextField p1, TextField p2){//Start
        SimulatorConfig.M = M.getValue();
        SimulatorConfig.N = (Double)N.getValue();
        SimulatorConfig.I = (Double)I.getValue();
        SimulatorConfig.P0 = Double.parseDouble(p0.getText());
        SimulatorConfig.P1 = Double.parseDouble(p1.getText());
        SimulatorConfig.P2 = Double.parseDouble(p2.getText());

        simulator.start();
        settings.setDisable(true);


    }
    public void SettingsDisable2(Menu settings, Simulator simulator){//Stop
        simulator.stop();
        settings.setDisable(false);
    }
}



