# [ELO329] [Tarea 2: Simulandor Gráfico de la Evolución de una Pandemia](https://gitlab.com/grupo-2-elo329/tarea_2)

## Integrantes
* [Jared Soto](https://gitlab.com/Jared677)
* [Philippe Pastene](https://gitlab.com/Philippe_)
* [Cristobal Gonzalez](https://gitlab.com/Cristobal.Gonzalezs)
* [Ignacio Gaete](https://gitlab.com/IgnacioGaete)

## Objetivo General

Esta tarea busca adaptar y extender la [Tarea 1](https://gitlab.com/grupo-2-elo329/Tarea1) para simular gráficamente una situación inspirada en una pandemia tipo Covid-19. De ninguna manera la intención es obtener resultados sobre cómo debería evolucionar una pandemia real. Además, otros objetivos del proyecto fueron:

* Modelar objetos reales como objetos de software.
* Ejercitar la creación y extensión de clases dadas para satisfacer nuevos requerimientos.
* Reconocer clases y relaciones entre ellas en códigos fuentes Java.
* Ejercitar la compilación y ejecución de programas en lenguaje JavaFX desde una consola de comandos.
* Ejercitar la configuración de un ambiente de trabajo para desarrollar aplicaciones en lenguaje Java.
* Manejar proyectos vía [GIT](https://git-scm.com/).
* Ejercitar la preparación y entrega de resultados de software (creación de makefiles, readme, documentación).
* Familiarización con desarrollos "iterativos" e "incrementales".

## Descripción de la Tarea

Se considera un número dado de **N** individuos representados por la clase *Individuo* dispersos que se mueven aleatoriamente y de manera confinada en una región plana rectangular llamada *Comuna* durante un tiempo **T** (tiempo de simulación). De los **N** individuos iniciales, **S** es el número de individuos susceptibles a infectarse e **I** es el número de individuos ya infectados. En este ejercicio los individuos infectados pasan a estado **R** (recuperados) al cabo de **I_time** segundos.

La *Comuna* es rectangular de dimensiones (**width**,**length**) y los movimientos de los individuos son aleatorios con rapidez **Speed**. La dirección **theta** de la velocidad varóa cada **dt** a un valor aleatorio tomado de entre **theta-dTheta** y **theta+dTheta**. Cuando un *Individuo* tiende a salir de la *Comuna*, éste cambia su dirección como si estuviera rebotando en el borde de ésta.

Cuando un *Individuo* susceptible de infectarse se acerca a una distancia inferior a **d** [m] de uno infectado, existe una probabilidad P de que adquiera la infección por cada segundo en contacto cercano. Esta probabilidad de contagio depende de si ningun (P=**p0**), solo uno (P=**p1**) o ambos (P=**p2**) usan mascarilla.

Finalmente, en la *Comuna* se activan **NumVac** *Vacunatorios* a los **VacTime** segundos de iniciada la simulación. Los *Vacunatorios* son representados como zonas cuadradas fijas de lado **VacSize** y ubicados aleatoriamente en la comuna. Cuando un *Individuo* susceptible ingresa al área de un *Vacunatorio*, éste es vacunado y deja de ser susceptible a la infección. *Individuos* infectados o recuperados no son vacunados en esta simulación.

## [Stage 1](https://gitlab.com/grupo-2-elo329/tarea_2/-/tree/master/Stage1): Individuo se mueve aleatoriamente
En esta etapa no hay vacunatorios, no se usa mascarilla y solo existe un *Individuo* que se mueve aleatoriamente en la *Comuna*. Las clases principales aquí son *Stage1*, *Simulador*, *Comuna* e *Individuo*.

La clase *Stage1* contiene el método main, crea una instancia de *Comuna*, una de *Individuo* y una de *Simulador* a partir del archivo de entrada. Esta clase es la encargada de correr la aplicación, manejando el avance del tiempo. La clase *Comuna* define el territorio y contiene al único individuo que se mueve en ella. La clase *Individuo* define la ubicación de los individuos dentro de la comuna y el movimiento de éstos dentro de ella.

## [Stage 2](https://gitlab.com/grupo-2-elo329/tarea_2/-/tree/master/Stage2): Varios Individuos se mueven y se contagian
Esta etapa es similar a la previa (sin vacuna, sin mascarilla), excepto que se crean **N** individuos los cuales son almacenados en un arreglo. La clase simulador crea y ubica **I** individuos infectados y (**N**-**I**) individuos susceptibles de infectarse. La clase *Individuo* se completa para reflejar la interacción entre individuos.

## [Stage 3](https://gitlab.com/grupo-2-elo329/tarea_2/-/tree/master/Stage3): Alguno de los Individuos pueden usar mascarilla
Esta etapa modifica la forma como los individuos se pueden contagiar dependiendo del uso de mascarilla, implementada como un atributo extra en la clase *Individuo*. Además se incorpora a la interfaz un gráfico de áreas apiladas que registra la eveolución de las variables de interés (**S**, **I** y **R**).

## [Stage 4](https://gitlab.com/grupo-2-elo329/tarea_2/-/tree/master/Stage4): Se crean Vacunatorios que inmunizan a Individuos de ser contagiados
En esta última etapa se logra incorporar todas las funcionalidades pedidas, es decir, individuos moviéndose aleatoriamente en la comuna, unos con mascarillas y otros no, se contagian entre ellos y además al cabo de un tiempo **VacTime** se generan **NumVac** vacunatorios los cuales vacunan aquellos individuos susceptibles que esten dentro de su región. En el gráfico también se reflejan esto cambios

## Interfaz gráfica

## Archivo de entrada
Cada etapa requiere de un archivo de entrada (**entrada.txt**) donde se establecen los parámetros de simulación. La estructura de este archivo es la siguiente:

**T**<*espacio*>**N**<*espacio*>**I**<*espacio*>**I_Time**

**width**<*espacio*>**length**

**speed**<*espacio*>**dt**<*espacio*>**dTheta**

**d**<*espacio*>**M**<*espacio*>**p0**<*espacio*>**p1**<*espacio*>**p2**

**NumVac**<*espacio*>**VacSize**<*espacio*>**VacTime**<*espacio*>

Si bien la estructura es rígida, el valor de cada parámetro puede ser cambiado sin ningun problema (**deben estar todos definidos incluso si su valor fuera cero**).

## Sobre como compilar y ejecutar cada etapa
En cada etapa existe un archivo *makefile* encargado de manejar la compilación, ejecución y limpieza de archivos .class generados. Por ejemplo, para compilar se utiliza `$make default JFX-PATH=/path/to/javafx/lib` (**se debe dar como argumento /path/to/javafx/lib que es el directorio de la librería de JavaFX**), para correr el progrma `$make run JFX-PATH=/path/to/javafx/lib` y para limpiar los archivos .class se ejecuta `$make clean`.
