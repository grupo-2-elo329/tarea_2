package Stage4;

public class Vacunatorio {
    double vacSize,x,y;
    private VacunatorioView view;

    public Vacunatorio(double x, double y, Comuna comuna){
        vacSize = SimulatorConfig.VAC_SIZE;
        this.x = x;
        this.y = y;
        view = new VacunatorioView(this, comuna);

    }


    public double getX(){
        return x;
    }
    public double getY(){
        return y;
    }

    public VacunatorioView getView() {
        return view;
    }

}
