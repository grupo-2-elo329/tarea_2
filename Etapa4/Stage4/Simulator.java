package Stage4;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.Group;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.util.Duration;

public class Simulator {
    boolean vacunatorios = true;
    private Timeline animation;
    private Comuna comuna;
    private SimulatorConfig config;
    private double simulationSamplingTime;
    private double simulationTime;  // it goes along with real time, faster or slower than real time
    private double delta_t;   // precision of discrete simulation time

    /**
     * @param framePerSecond frequency of new views on screen
     * @param simulationTime2realTimeRate how faster the simulation runs relative to real time
     */
    public Simulator (double framePerSecond, double simulationTime2realTimeRate,Comuna comuna, SimulatorConfig config){
        this.comuna = comuna;
        this.config=config;
        double viewRefreshPeriod = 1 / framePerSecond; // in [ms] real time used to display
        // a new view on application
        simulationSamplingTime = viewRefreshPeriod *simulationTime2realTimeRate;
        delta_t = SimulatorConfig.DELTA_T;
        simulationTime = 0;
        animation = new Timeline(new KeyFrame(Duration.millis(viewRefreshPeriod*1000), e->takeAction()));
        animation.setCycleCount(Timeline.INDEFINITE);
    }
    private void takeAction() {
        double nextStop = simulationTime + simulationSamplingTime;
        for (; simulationTime < nextStop; simulationTime += delta_t) {
            comuna.computeNextState(delta_t, simulationTime); // compute its next state based on current global state
            comuna.updateState();            // update its state
        }
        comuna.updater(simulationTime);
        comuna.updateView(); //done

        if (vacunatorios && simulationTime > SimulatorConfig.VAC_TIME ){
            setVac();
            vacunatorios = false;

        }
    }
    public void start(){
        this.simulationTime=0;
        comuna.personrefresh();
        vacunatorios = true;

        animation.play();
        comuna.getView().setOnKeyPressed( e->keyHandle(e));
        setPerson(config);

    }
    private void keyHandle (KeyEvent e) {
        if(e.getCode() == KeyCode.LEFT) slowdown();
        if(e.getCode() == KeyCode.RIGHT) speedup();//done
    }
    public void stop(){
        animation.stop();
        animation.jumpTo(Duration.ZERO);

    }
    public void speedup(){
        delta_t*=2;//done
    }
    public void slowdown(){
        if(delta_t>0.00001)
            delta_t*=0.5; //done
    }
    public void setPerson(SimulatorConfig SimulatorConfig){
        int contador=0;
        double speed = SimulatorConfig.SPEED;//done
        double deltaAngle = SimulatorConfig.DELTA_THETA;//done
        Pedestrian persona;
        for (int i=0; i < SimulatorConfig.I;i++) {
            if (contador < Math.round(SimulatorConfig.I*SimulatorConfig.M)) {

                persona = new Pedestrian(comuna, speed, deltaAngle, 1, true);

                contador+=1;
            } else {
                persona = new Pedestrian(comuna, speed, deltaAngle, 1, false);

            }
            this.comuna.setPerson(persona);
        }
        for (int i=(int)SimulatorConfig.I; i < SimulatorConfig.N;i++){
            if(contador < Math.round((SimulatorConfig.N-SimulatorConfig.I)*SimulatorConfig.M)){
                persona = new Pedestrian(comuna, speed, deltaAngle, 0, true);
                contador+=1;
            }else{
                persona = new Pedestrian(comuna, speed, deltaAngle, 0, false);
            }
            this.comuna.setPerson(persona);

        }
    }

    public Group getView(){ return comuna.getView();}
    public Pane getGraph(){ return comuna.getGraph();}


    public void setVac(){
        for (int i = 0; i < SimulatorConfig.NUM_VAC; i++) {
            vacPos();
        }
    }

    public void vacPos(){
        double x = SimulatorConfig.VAC_SIZE / 2 + Math.random() * (SimulatorConfig.WIDTH - SimulatorConfig.VAC_SIZE);
        double y = SimulatorConfig.VAC_SIZE / 2 + Math.random() * (SimulatorConfig.LENGTH - SimulatorConfig.VAC_SIZE);
        if(comuna.getVacunatorio().size() == 0) {
            Vacunatorio vac = new Vacunatorio(x,y,comuna);
            this.comuna.setVac(vac);
            comuna.getView().getChildren().add(vac.getView());
        } else {
            int i = 0;
            while(i < comuna.getVacunatorio().size()){
                i = 0;
                x = SimulatorConfig.VAC_SIZE / 2 + Math.random() * (SimulatorConfig.WIDTH - SimulatorConfig.VAC_SIZE);
                y = SimulatorConfig.VAC_SIZE / 2 + Math.random() * (SimulatorConfig.LENGTH - SimulatorConfig.VAC_SIZE);
                for(Vacunatorio item:comuna.getVacunatorio()){
                    if(Math.abs(item.getX() - x) > SimulatorConfig.VAC_SIZE && Math.abs(item.getY() - y) > SimulatorConfig.VAC_SIZE){
                        i++;
                    }
                }
            }
            Vacunatorio vac = new Vacunatorio(x, y, comuna);
            this.comuna.setVac(vac);
            comuna.getView().getChildren().add(vac.getView());


        }
    }

}

