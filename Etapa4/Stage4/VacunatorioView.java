package Stage4;

import javafx.scene.Group;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import java.awt.*;

public class VacunatorioView extends Group {
    private Vacunatorio vac;
    private Comuna comuna;
    private Rectangle territoryView;
    VacunatorioView(Vacunatorio vac, Comuna comuna){
        this.vac = vac;
        this.territoryView = new Rectangle(SimulatorConfig.VAC_SIZE, SimulatorConfig.VAC_SIZE,
                javafx.scene.paint.Color.rgb(135,206,235));
        territoryView.setStroke(Color.BLUE);
        territoryView.setX(vac.getX() - SimulatorConfig.VAC_SIZE/2);
        territoryView.setY(vac.getY() - SimulatorConfig.VAC_SIZE/2);
        this.comuna = comuna;
        comuna.getView().getChildren().add(territoryView);
    }
}