package Stage4;

import javafx.scene.Group;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.StackedAreaChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class ComunaView extends Group{
    private final Comuna comuna;
    Rectangle territoryView;
    XYChart.Series<Number,Number>  seriesSanos;
    XYChart.Series<Number,Number>  seriesInfectados;
    XYChart.Series<Number,Number>  seriesRecuperados;
    XYChart.Series<Number,Number>  seriesVacunados;
    NumberAxis xAxis;
    NumberAxis yAxis;
    StackedAreaChart<Number,Number> areas;

    public ComunaView(Comuna c){
        comuna = c;
        this.territoryView = new Rectangle(comuna.getWidth(), comuna.getHeight(), Color.WHITE);
        territoryView.setStroke(Color.BROWN);
        getChildren().add(territoryView);
        setFocusTraversable(true);  // needed to receive mouse and keyboard events.
        xAxis=new NumberAxis();
        yAxis=new NumberAxis();
        areas = new StackedAreaChart<Number,Number>(xAxis,yAxis);
        areas.setCenterShape(true);
        areas.setTitle("Evolucion de la pandemia by poo");
        areas.setStyle("CHART_COLOR_1: rgb("+0+","+0+","+255+");"+"CHART_COLOR_1_TRANS_20: rgba("+0+","+0+","+150+");"+
                "CHART_COLOR_2: rgb("+255+","+0+","+0+");"+"CHART_COLOR_2_TRANS_20: rgba("+150+","+0+","+0+");"+
                "CHART_COLOR_3: rgb("+194+","+130+","+98+");"+"CHART_COLOR_3_TRANS_20: rgba("+128+","+64+","+0+");"+
                "CHART_COLOR_4: rgb("+0+","+255+","+0+");"+"CHART_COLOR_4_TRANS_20: rgba("+0+","+150+","+0+");");
        areas.setAnimated(false);
        comuna.getGraph().getChildren().add(areas);
        seriesSanos= new XYChart.Series<Number,Number> ();
        seriesSanos.setName("Sanos");
        seriesSanos.getData().add(new XYChart.Data<Number,Number>(0, SimulatorConfig.N- SimulatorConfig.I));
        seriesInfectados= new XYChart.Series<Number,Number> ();
        seriesInfectados.setName("Infectados");
        seriesInfectados.getData().add(new XYChart.Data<Number,Number>(0, SimulatorConfig.I));
        seriesRecuperados= new XYChart.Series<Number,Number> ();
        seriesRecuperados.setName("Recuperados");
        seriesRecuperados.getData().add(new XYChart.Data<Number,Number>(0, 0));
        seriesVacunados= new XYChart.Series<Number,Number> ();
        seriesVacunados.setName("Vacunados");
        seriesVacunados.getData().add(new XYChart.Data<Number,Number>(0, 0));
        areas.getData().addAll(seriesSanos, seriesInfectados, seriesRecuperados, seriesVacunados);

    }
    public void update(){
        for (int i = 0; i< SimulatorConfig.N; i++) {
            comuna.getPedestrian(i).updateView(comuna);//done
        }
    }

    public Rectangle getTerritoryView() {
        return territoryView;
    }

    public void  refresh(Rectangle prev_territory){
        getChildren().remove(prev_territory);
        this.territoryView = new Rectangle(comuna.getWidth(), comuna.getHeight(), Color.WHITE);
        territoryView.setStroke(Color.BROWN);
        getChildren().add(territoryView);
        setFocusTraversable(true);  // needed to receive mouse and keyboard events.
        seriesSanos.getData().clear();
        seriesSanos.getData().add(new XYChart.Data<Number,Number>(0, SimulatorConfig.N- SimulatorConfig.I));
        seriesInfectados.getData().clear();
        seriesInfectados.getData().add(new XYChart.Data<Number,Number>(0, SimulatorConfig.I));
        seriesRecuperados.getData().clear();
        seriesRecuperados.getData().add(new XYChart.Data<Number,Number>(0, 0));
        seriesVacunados.getData().clear();
        seriesVacunados.getData().add(new XYChart.Data<Number,Number>(0,0));
    }

    public void updater(double t) {
        seriesSanos.getData().add(new XYChart.Data<Number,Number>(t, comuna.getSanos()));
        seriesInfectados.getData().add(new XYChart.Data<Number,Number>(t, comuna.getInfectados()));
        seriesRecuperados.getData().add(new XYChart.Data<Number,Number>(t, comuna.getRecuperados()));
        seriesVacunados.getData().add(new XYChart.Data<>(t, comuna.getVacunados()));
    }
}
