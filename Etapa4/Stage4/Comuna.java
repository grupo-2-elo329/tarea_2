package Stage4;

import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.layout.Pane;
import java.util.ArrayList;

public class Comuna {
    private ArrayList <Pedestrian> person = new ArrayList<Pedestrian>();
    private ArrayList <Vacunatorio> vacs = new ArrayList<Vacunatorio>();
    private Rectangle2D territory; // Alternatively: double width, length;
    private ComunaView view;
    private Pane graph;
    boolean vacunado;

    public Comuna(SimulatorConfig SimulatorConfig){
        double width = SimulatorConfig.WIDTH;
        double length = SimulatorConfig.LENGTH; //done
        territory = new Rectangle2D(0,0, width, length);
        double speed = SimulatorConfig.SPEED;//done
        double deltaAngle = SimulatorConfig.DELTA_THETA;//done
        graph = new Pane();  // to be completed in other stages.
        view = new ComunaView(this); // What if you exchange this and the follow line?
        //ArrayList<Pedestrian> person = new ArrayList<Pedestrian>();
        vacunado = false;

    }
    public void setPerson(Pedestrian persona) {
        person.add(persona);
    }
    public double getWidth() {
        return territory.getWidth();
    }
    public double getHeight() {
        return territory.getHeight();
    }
    public void computeNextState (double delta_t, double t) {
        for(Pedestrian item: person) {
            item.computeNextState(delta_t);
            vacunado = item.vacunar(vacs);
            item.computeNextStatus(person, SimulatorConfig.D, t, delta_t, SimulatorConfig.I_TIME,
                    SimulatorConfig.P0, SimulatorConfig.P1, SimulatorConfig.P2, vacunado);
        }
    }
    public void updateState () {
        for(Pedestrian item: person) {
            item.updateState();
        }
    }
    public void updateView(){
        view.update();
    }
    public Pedestrian getPedestrian(int i) {
        return person.get(i);
    }
    public Group getView() {
        return view;
    }
    public Pane getGraph(){
        return graph;
    }
    public void personrefresh(){ view.refresh(view.getTerritoryView());this.person=new ArrayList<Pedestrian>();}
    public int getSanos(){
        int i = 0;
        for(Pedestrian item: person){
            if(item.getStatus() == 0) i++;
        }
        return i;
    }
    public int getInfectados(){
        int i = 0;
        for(Pedestrian item: person){
            if(item.getStatus() == 1) i++;
        }
        return i;
    }
    public int getRecuperados(){
        int i = 0;
        for(Pedestrian item: person){
            if(item.getStatus() == 2) i++;
        }
        return i;
    }
    public int getVacunados(){
        int i = 0;
        for(Pedestrian item: person){
            if(item.getStatus() == 3) i++;
        }
        return i;
    }
    public void setVac(Vacunatorio vac){
        vacs.add(vac);
    }
    public ArrayList<Vacunatorio> getVacunatorio(){return vacs;}
    public void updater(double t){view.updater(t);}
}