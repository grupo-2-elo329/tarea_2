package Stage4;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.io.File;
import java.util.ArrayList;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.io.File;
import java.util.ArrayList;

public class Pedestrian {
    private double x, y, speed, angle, deltaAngle;
    private double x_tPlusDelta, y_tPlusDelta;
    private Comuna comuna;
    private PedestrianView pedestrianView;
    private double t_infectado;
    private int status, nextStatus; // 0-> sano y no recuperado, 1-> infectado ,2-> sano y recuperado, 3 vacunado
    private boolean mask;

    public Pedestrian(Comuna comuna, double speed, double deltaAngle, int status, boolean mask){
        t_infectado=0;
        this.comuna = comuna;
        this.speed = speed;
        this.deltaAngle=deltaAngle;
        this.mask=mask;
        x = Math.random()*comuna.getWidth();
        y = Math.random()*comuna.getHeight();
        angle = Math.random()*2*Math.PI;
        this.status = status;
        this.nextStatus = this.status;// MUY IMPORTANTE QUE ESTAS LINEAS VAYAN ANTES DEL VIEW
        pedestrianView = new PedestrianView(comuna, this);//done

    }
    public double getX(){
        return x;
    }
    public double getY() {
        return y;
    }
    public void computeNextState(double delta_t) {
        double r=Math.random();
        angle+=deltaAngle*(1-2*r);
        x_tPlusDelta=x+speed*Math.cos(angle)*delta_t;
        y_tPlusDelta=y+speed*Math.sin(angle)*delta_t;
        if(x_tPlusDelta < 0){   // rebound logic
            x_tPlusDelta=-x_tPlusDelta;
            angle=Math.PI-angle;
        }
        if(y_tPlusDelta < 0){
            y_tPlusDelta=-y_tPlusDelta;
            angle=2*Math.PI-angle;
        }
        if( x_tPlusDelta > comuna.getWidth()){
            x_tPlusDelta=2*comuna.getWidth()-x_tPlusDelta;
            angle=Math.PI-angle;
        }
        if(y_tPlusDelta > comuna.getHeight()){
            y_tPlusDelta=2*comuna.getHeight()-y_tPlusDelta;
            angle=2*Math.PI-angle;
        }
    }

    public void computeNextStatus(ArrayList<Pedestrian> lista, double distancia,
                                  double  t_actual, double delta_t,
                                  double t_recuperacion, double proba0, double p1, double p2,
                                  boolean vacunado){
        if(status==1){
            if (this.recuperacion(t_recuperacion,t_actual,delta_t)){nextStatus=2;}
        }
        if(this.Infeccion(lista, t_actual, distancia, proba0, p1, p2)){
            nextStatus=1;
        }
        if(vacunado){
            nextStatus = 3;
        }

    }



    public void updateState(){
        x=x_tPlusDelta;
        y=y_tPlusDelta;
        status=nextStatus;

    }

    public PedestrianView getView(){ return pedestrianView;}

    public void updateView(Comuna comuna) {
        pedestrianView.update(comuna);//done
    }

    public int getStatus(){return status;}
    public int getNextStatus(){return nextStatus;}

    private boolean recuperacion(double I_time, double t, double delta_t){
        if((t+delta_t)-t_infectado>=I_time){return true;}
        else {return false;}
    }

    private int radar(ArrayList<Pedestrian> lista, double x, double y, double d){
        double x_prima = x;
        double y_prima = y;
        double diff_x, diff_y;
        int j=0;
        for(int i = 0; i < lista.size(); i++){
            if(lista.get(i).getStatus() == 1){
                diff_x=lista.get(i).getX()-x_prima;
                diff_y=lista.get(i).getY()-y_prima;
                if(diff_x*diff_x+diff_y*diff_y <= d*d) {
                    j++;
                }
            }
        }
        return j;
    }
    public boolean getMask(){return mask;}
    private int radarmask(ArrayList<Pedestrian> lista, double x, double y, double d){
        double x_prima = x;
        double y_prima = y;
        double diff_x, diff_y;
        int j=0;
        for(int i = 0; i < lista.size(); i++){
            if(lista.get(i).getStatus() == 1){
                diff_x=lista.get(i).getX()-x_prima;
                diff_y=lista.get(i).getY()-y_prima;
                if(diff_x*diff_x+diff_y*diff_y <= d*d) {
                    if(lista.get(i).getMask()){
                        j++;
                    }
                }
            }
        }
        return j;
    }

    private boolean Infeccion(ArrayList<Pedestrian> lista, double t, double d, double p0, double p1, double p2){
        if (status == 0) {
            int c, m;
            int i;
            c = this.radar(lista,x_tPlusDelta,y_tPlusDelta,d);
            if(c >0){
                m = this.radarmask(lista,x_tPlusDelta,y_tPlusDelta,d);
                for(i=0 ; i<m; i++){
                    if(mask){
                        if(Math.random() < p2) {
                            t_infectado=t;
                            playinfected();
                            return true;
                        }
                    }
                    else{
                        if(Math.random() < p1) {
                            t_infectado=t;
                            playinfected();
                            return true;
                        }
                    }
                }
                for(i=m; i<c; i++) {
                    if (Math.random() < p0) {
                        t_infectado = t;
                        playinfected();
                        return true;
                    }
                }

            }
        }
        return false;
    }

    public boolean vacunar(ArrayList<Vacunatorio> vacs){
        if(this.getStatus() == 0) {
            double x_pos = this.getX();
            double y_pos = this.getY();
            for (Vacunatorio item: vacs) {
                if (x_pos > item.getX() && y_pos > item.getY()) {
                    if (x_pos < item.getX() + SimulatorConfig.VAC_SIZE
                            && y_pos < item.getY() + SimulatorConfig.VAC_SIZE ) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private void playinfected(){
        Media sound = new Media(new File("Stage4/tuki.mp4").toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(sound);
        mediaPlayer.setVolume(1);
        mediaPlayer.play();
    }

}
